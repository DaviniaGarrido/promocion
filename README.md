# README #


### ¿Qué hay en este repositorio? ###

* Este repositorio se ha creado para presentar una Landing Page con motivo de promocionar un nuevo coche de la marca Opel.

* Version 1.1

### Funcionamiento ###

* Será necesario instalar un programa como Netbeans para importar el proyecto y poder hacer modificaciones de código.
* También es necesario tener un servidor local que soporte PHP para poder ver la web en el navegador.
* Se deberá crear una base de datos en el servidor.
* El proyecto se lanza ejecutando el archivo index.php

### Contacto ###

* Davinia Garrido
* dmgl0002@red.ujaen.es