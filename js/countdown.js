// Selecciona la fecha fina
var countDownDate = new Date("Sep 3, 2017 15:37:00").getTime();

// Actualizamos cada 1 segundo
var x = setInterval(function () {

    // Cogemos la fecha y hora de hoy
    var now = new Date().getTime();

    // Calculamos el tiempo restante entre hoy y la fecha final
    var distance = countDownDate - now;

    // Calculos para días, horas, minutos y segundos
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Display
    document.getElementById("cuenta").innerHTML = days + "d " + hours + "h "
            + minutes + "m " + seconds + "s ";

    // Si la cuenta atras ha terminado, se pone FIN
    if (distance < 0) {
        clearInterval(x);
        document.getElementById("cuenta").innerHTML = "FIN";
    }
}, 1000);