/**Función fara hacer desplegables dependientes**/
var Turismo_Comercial = new Array();
var TodoTerreno = new Array();
Turismo_Comercial[0] = "corsa";
Turismo_Comercial[1] = "astra";
TodoTerreno[0] = "mokka";
TodoTerreno[1] = "crossland";

function direccion(form) {
    var selec = form.tipovehiculo.options;
    var combo = form.vehiculo.options;
    if (selec[1].selected == true) {
        document.form.action = Turismo_Comercial[combo.selectedIndex];
    }
    if (selec[2].selected == true) {
        document.form.action = TodoTerreno[combo.selectedIndex];
    }
    if (selec[3].selected == true) {
        document.form.action = Turismo_Comercial[combo.selectedIndex];
    }

    /*Se puede quitar una vez vemos que funciona*/
    alert(form.action);
}

function agregarOpciones(form) {
    var selec = form.tipovehiculo.options;
    var combo = form.vehiculo.options;
    combo.length = null;

    if (selec[0].selected == true) {
        var seleccionar = new Option("Esperando selección");
        combo[0] = seleccionar;
    }

    if (selec[1].selected == true) {
        var coche1 = new Option("Corsa");
        var coche2 = new Option("Astra");
        combo[0] = coche1;
        combo[1] = coche2;
    }

    if (selec[2].selected == true) {
        var coche3 = new Option("Mokka");
        var coche4 = new Option("Crossland");
        combo[0] = coche3;
        combo[1] = coche4;
    }

    if (selec[3].selected == true) {
        var coche1 = new Option("Corsa");
        var coche2 = new Option("Astra");
        combo[0] = coche1;
        combo[1] = coche2;
    }
}



/**Función para que cambien las imágenes de la web cada x segundos**/
var imagenes = new Array(
        'images/img1.jpg',
        'images/img2.jpg',
        'images/img3.jpg',
        );

/*Funcion para cambiar la imagen*/
function rotarImagenes() {
    // obtenemos un numero aleatorio entre 0 y la cantidad de imagenes que hay
    var index = Math.floor((Math.random() * imagenes.length));
    // cambiamos la imagen
    document.getElementById("imagen").src = imagenes[index];
}

/* Función que se ejecuta una vez cargada la página */
onload = function () {
    // Cargamos una imagen aleatoria
    rotarImagenes();
    // Indicamos que cada 5 segundos cambie la imagen
    setInterval(rotarImagenes, 5000);
};



/** Spinner **/
$('button#boton').on('click', function (e) {
    var $load = $("#loader").show();

    $.ajax({
        data: datos,
        url: 'form2.php',
        success: function (data) { // Aquí desaparece la imagen ya que estamos reemplazando todo el HTML del contenido de la div. 
            $load.html(data);
        }
    });
    e.preventDefault();
});