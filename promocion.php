<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Promocion</title>
        <link href="CSS/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="CSS/style.css" rel="stylesheet" type="text/css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.bootstrapvalidator/0.5.3/js/bootstrapValidator.js"></script>
        <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.23/angular.min.js"></script>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div id="contenedor">

            <header>
                <img src="" id="imagen">
                <div><p>Prepárate para volar por carretera</p></div>
            </header>


            <section id="datos">
                <h2 id="contacto">¿Quieres saber más? No lo dudes, te llamamos nosotros</h2>
                <form class="formulario" role="form" action="form2.php" method="post" ajax="true" name='formulario' onsubmit="document.forms['formulario']['enviar'].disabled = true;">

                    <fieldset>

                        <div class="form-group">      
                            <div class="col-md-6">
                                <label class="control-label">Nombre (*)</label>
                                <input type="text" class="form-control" id="name" placeholder="Nombre" name="nombre" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <label class="control-label">Apellidos (*)</label>

                                <input type="text" class="form-control" id="surname" placeholder="Apellidos" name="apellidos" required>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6">
                                <label class="control-label">Telefono</label>
                                <input type="tel" pattern="[6-8]{1}[0-9]{8}" 
                                       title="Un número de teléfono válido debe empezar por 6, 7 u 8 y tener 9 dígitos en total" 
                                       class="form-control" id="phone" placeholder="Telefono" name="telefono">
                            </div>  
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <label class="control-label">Email (*)</label>

                                <input type="email" class="form-control" id="email" placeholder="Email" name="email" required>
                            </div>
                        </div>      




                        <div class="form-group">
                            <div class="col-md-6">
                                <label class="control-label">Tipo de vehículo (*)</label>

                                <select class="form-control" name="tipovehiculo" onChange="agregarOpciones(this.form)" id="vehicle_tipe"  required>
                                    <option value="" selected="selected">- Selecciona -</option>
                                    <option value="Turismo">Turismo</option>
                                    <option value="Todo_terreno">Todo terreno</option>
                                    <option value="Comercial">Comercial</option>
                                </select>
                            </div>
                        </div> 

                        <div class="form-group">
                            <div class="col-md-6">
                                <label class="control-label">Vehículo (*)</label>

                                <select class="form-control" name="vehiculo" onChange="direccion(this.form)" id="vehicle"  required>
                                    <option value="">- Esperando seleccion -</option>
                                </select>
                            </div>
                        </div>   


                        <div class="form-group">
                            <div class="col-md-6">
                                <label class="control-label">Preferencia de llamada (*)</label>
                                <select class="form-control" id="call" name="llamada" required disabled="true"

                                        <?php
                                        $variable = $_GET['preferencia'];
                                        $options = array(
                                            'manana',
                                            'tarde',
                                            'noche',
                                        );
                                        $output = '';
                                        for ($i = 0; $i < count($options); $i++) {
                                            $output .= '<option '
                                                    . ( $variable == $options[$i] ? 'selected="selected"' : '' ) . '>'
                                                    . $options[$i]
                                                    . '</option>';
                                        }
                                        ?>
                                        <option value="manana">Mañana</option>
                                    <option value="tarde">Tarde</option>
                                    <option value="noche">Noche</option>
                                </select>



                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-lg btn-block info" id="boton" name='enviar'>Entrar</button>
                                <label class="carga"><img id="loader" src="images/ajax-loader.gif"/></label>
                            </div>
                        </div>

                        <div id='load'>
                        </div>

                    </fieldset>
                </form>


            </section><!-- datos close -->
            <section id="info">
                <h2>AHORA O NUNCA</h2>
                <h3>Nuevo <span>Opel Insignia Innovative Diésel 136 cv</span> por</h3>
                <p class="parpadea text">21990 €</p>

                <div class='columnas'>
                    <div class="col1"><img src="images/img3.jpg" alt=""/></div>

                    <div class='col2'>
                        <h4>con 6000 € del equipamiento más tecnológico</h4>
                        <div class="caracteristicas">
                            <p>Faros adaptativos inteligentes AFL Plus</p>
                            <p>Sistema Multimedia con navegación</p>
                            <p>Cámara de Visión Trasera</p>
                            <p>Asistente de aparcamiento delantera y trasera</p>
                            <p>LLantas de aleación de 18''</p>


                        </div>
                    </div>
                </div>

                <p class='validez'>Validez de la oferta hasta el 10/09/2017 en compras con financiación</p>

            </section>

            <section id="countdown">
                <h2>No dejes que acabe el tiempo...</h2>

                <p id="cuenta"></p>
                <a href="#contacto"><button class="boton">Contacta con nosotros</button></a>

            </section>

            <footer>
                <a>Nota legal</a>
                <a>Opel 2017</a>
                <a>Política de privacidad y cookies</a>
            </footer>

        </div><!-- contenedor close -->
        <script src="js/javascript.js" type="text/javascript"></script>
        <script src="js/countdown.js" type="text/javascript"></script>
    </body>
</html>
